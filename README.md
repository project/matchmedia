INTRODUCTION
------------

Drupal added the matchmedia asset library in 2012 as a polyfill for browsers
that do not support matchMedia and MediaQueryList event listeners

In 2019 the browser support policy was updated, there are no longer any browsers
that require this polyfill.

The core asset libraries core/matchmedia and core/matchmedia.addListener are
deprecated in Drupal 8.8 and removed in Drupal 9.

See: https://www.drupal.org/node/3086653

This module provides the same asset library for any sites which continue to need
support for older browsers.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * In your theme or module attach the matchMedia/matchMedia asset library. Visit 
   https://www.drupal.org/docs/8/api/javascript-api/add-javascript-to-your-theme-or-module
   for further information.

MAINTAINERS
-----------

Current maintainers:
 * Peter Weber (zrpnr) - https://www.drupal.org/u/zrpnr

